import { createRequire } from "module";
const require = createRequire(import.meta.url);

const r = require('raylib');


import {clicked_at} from "./utils.mjs";
import {frame_get_property, PROPERTY_MOUSE_STATE, MOUSE_PROPERTY_LEFT_BUTTON_PRESSED, debug_print_frame} from "./frame.mjs";

export function button(x,y, width, height) {
	if (clicked_at(x,y, width, height)) {
		console.log("Olá, mundo!");
	}
	r.DrawRectangle(x,y, width,height, r.BLACK);
}
