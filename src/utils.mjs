import { createRequire } from "module";
const require = createRequire(import.meta.url);
const r = require('raylib');

import {frame_get_property, MOUSE_PROPERTY_LEFT_BUTTON_PRESSED, PROPERTY_MOUSE_STATE} from "./frame.mjs"

export function clicked_at(x, y, width, height){
	let old_mouse_state = frame_get_property(PROPERTY_MOUSE_STATE);

	let current_mouse_x = r.GetMouseX();
	let current_mouse_y = r.GetMouseY();

	let is_mouse_on_right_pos = (current_mouse_x >= x && current_mouse_x <= x + width) && (current_mouse_y >= y && current_mouse_y <=  y + height);
	
	let click_happened = old_mouse_state[MOUSE_PROPERTY_LEFT_BUTTON_PRESSED] == false && r.IsMouseButtonReleased(r.MOUSE_BUTTON_LEFT);


	return is_mouse_on_right_pos && click_happened;
}
