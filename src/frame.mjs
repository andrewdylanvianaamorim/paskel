'use strict';
import { createRequire } from "module";
const require = createRequire(import.meta.url);
const r = require('raylib');

//vai conter todas as informações necessárias para lidar com os eventos e estado entre frames
//vai ser responsabilidade de cada widget atualizar o frame(posições e menssagens)
let _frame = {
	x: 0,
	y: 0,
	width: 0,
	height: 0,
	mouse: {
		left_button_pressed: false, 
		right_button_pressed: false,
		left_button_released: false,
		right_button_released: false,
		x:0,
		y:0
	},
	messages: []
};

export const PROPERTY_X = "x";
export const PROPERTY_Y = "y";
export const PROPERTY_WIDTH = "width";
export const PROPERTY_HEIGHT = "height";
export const PROPERTY_MOUSE_STATE = "mouse";
export const MOUSE_PROPERTY_LEFT_BUTTON_PRESSED = "left_button_pressed";
export const MOUSE_PROPERTY_RIGHT_BUTTON_PRESSED = "right_button_pressed";
export const MOUSE_PROPERTY_LEFT_BUTTON_RELEASED = "left_button_released";
export const MOUSE_PROPERTY_RIGHT_BUTTON_RELEASED = "right_button_released";
export const MOUSE_PROPERTY_POSITION_X = "x";
export const MOUSE_PROPERTY_POSITION_Y = "y";

const PROPERTY_MESSAGES = "messages";


export function frame_get_property(property) {
	return _frame[property];
}

export function frame_set_property(property, property_value) {
	_frame[property] = property_value;
}

// deve ser chamado no final de cada frame
// tenha cuidado se o estado anterior será perdido se não tiver sido previamente salvo
function frame_update_mouse_state() {
	_frame[PROPERTY_MOUSE_STATE][MOUSE_PROPERTY_LEFT_BUTTON_PRESSED] = r.IsMouseButtonPressed(r.MOUSE_BUTTON_LEFT);
	_frame[PROPERTY_MOUSE_STATE][MOUSE_PROPERTY_RIGHT_BUTTON_PRESSED] = r.IsMouseButtonPressed(r.MOUSE_BUTTON_RIGHT);
	_frame[PROPERTY_MOUSE_STATE][MOUSE_PROPERTY_LEFT_BUTTON_RELEASED] = r.IsMouseButtonReleased(r.MOUSE_BUTTON_LEFT);
	_frame[PROPERTY_MOUSE_STATE][MOUSE_PROPERTY_RIGHT_BUTTON_RELEASED] = r.IsMouseButtonReleased(r.MOUSE_BUTTON_RIGHT);
	_frame[PROPERTY_MOUSE_STATE][MOUSE_PROPERTY_POSITION_X] = r.GetMouseX();
	_frame[PROPERTY_MOUSE_STATE][MOUSE_PROPERTY_POSITION_Y] = r.GetMouseY();
}

export function prepare_to_new_frame() {
	_frame[PROPERTY_MESSAGES] = [];
	frame_update_mouse_state();
}

export function debug_print_frame() {
	console.log(_frame);
}
