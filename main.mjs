'use strict';
import { createRequire } from "module";
const require = createRequire(import.meta.url);
const r = require("raylib");
import {prepare_to_new_frame, } from "./src/frame.mjs";
import {button} from "./src/button.mjs";


const screenWidth = 800
const screenHeight = 450
r.InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window")
r.SetTargetFPS(60)

while (!r.WindowShouldClose()) {


    r.BeginDrawing();
    r.ClearBackground(r.RAYWHITE)
    r.DrawText("Congrats! You created your first node-raylib window!", 120, 200, 20, r.LIGHTGRAY)
    button(200,200,150,30);
    r.EndDrawing()
    prepare_to_new_frame();
}
r.CloseWindow()
